package routes

import (
	"net/http"
	"time"
	"web_app/controller"
	_ "web_app/docs" // 千万不要忘了导入把你上一步生成的docs
	"web_app/logger"
	"web_app/middlewares"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	gs "github.com/swaggo/gin-swagger"
)

func SetupRouter() *gin.Engine {

	//New returns a new blank Engine instance without any middleware attached.
	r := gin.New()
	//使用logger中定义的方法 来替换默认的中间件 middlewares.RateLimitMiddleware(10*time.Second, 1)每10s往桶里添加1个令牌
	r.Use(logger.GinLogger(), logger.GinRecovery(true), middlewares.RateLimitMiddleware(1*time.Second, 1))
	r.GET("/swagger/*any", gs.WrapHandler(swaggerFiles.Handler))
	v1 := r.Group("api/v1")

	//注册业务 路由
	v1.POST("/register", controller.RegisterHandler)
	//r.GET("/select", controller.SelectHandler)
	//登录业务
	v1.POST("/login", controller.LoginHandler)
	v1.GET("/community", controller.CommunityHandler)
	v1.GET("/community/:id", controller.CommunityDetailHandler)
	v1.GET("/post/:id", controller.GetPostDetailHandler)
	v1.GET("/post/list/createTime", controller.GetPostListHandler)
	v1.GET("/post/list/parameter", controller.GetPostListByParameterHandler)
	//查看指定帖子的评论列表
	v1.GET("/comment/:id", controller.CommentListHandler)
	v1.Use(middlewares.JWTAuthMiddleware()) //应用JWT认证中间件

	{

		v1.POST("/post", controller.CreatePostHandler)
		v1.POST("/vote", controller.PostVoteHandler)
		v1.POST("/comment", controller.CommentHandler)

	}
	//只有登录状态的用户 才能访问该路径（判断请求头中是否有 有效的token）
	//v1.GET("/", middlewares.JWTAuthMiddleware(), func(c *gin.Context) {
	//	c.String(http.StatusOK, "ok")
	//})

	r.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"msg": "404",
		})
	})
	return r

}
