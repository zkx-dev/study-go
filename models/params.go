package models

//定义请求的参数结构体
const (
	Time  string = "time"
	Score string = "score"
)

//用户注册
type ParamUserRegister struct {
	Username   string `json:"username" binding:"required"`
	Password   string `json:"password" binding:"required"`
	RePassword string `json:"re_password" binding:"required,eqfield=Password"`
}

//用户登录
type ParamUserLogin struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

//用户对帖子投票数据
type ParamVoteData struct {
	//userId 从请求中获取当前用户id
	PostId string `json:"post_id" binding:"required"` //帖子id
	//oneof validator中的功能 ong of = xxx,yyy 在进行检验时 oneof修饰的字段 值 只能为 xxx或yyy
	Direction int8 `json:"direction,string"` //赞成票(1) 反对票(-1) 取消投票(0)
}
