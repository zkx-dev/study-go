package models

type Pagination struct {
	Limit       int64  `json:"limit" form:"limit" uri:"limit"`
	Page        int64  `json:"page" form:"page" uri:"page"`
	Sort        string `json:"sort" form:"sort" uri:"sort"`
	CommunityId int64  `json:"communityId" form:"communityId" uri:"communityId" db:"community_id"`
}
