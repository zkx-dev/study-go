package models

import (
	"time"

	"gorm.io/gorm"
)

type Post struct {
	PostId      uint64    `json:"id,string" db:"post_id"`
	AuthorId    uint64    `json:"author_id" db:"author_id"`
	CommunityId int64     `json:"community_id" db:"community_id" binding:"required"`
	Title       string    `json:"title" db:"title" binding:"required"`
	Content     string    `json:"content" db:"content" binding:"required"`
	CreatedAt   time.Time `json:"create_time" db:"created_at"`
	UpdatedAt   time.Time `json:"update_time" db:"updated_at"`
	//如果之后报错 此处json原来为 `json:"delete_time"`
	DeletedAt gorm.DeletedAt `json:"-" db:"deleted_at"`
}

//需要注意的是 结构体字段名定义是
/**
数据库中 username -> 结构体 Username
数据库中 community_name -> 结构体 CommunityName
*/
//帖子详情（帖子，社区名，作者名）结构体
//type PostDetail struct {
//	PostId        uint64         `json:"id" db:"post_id"`
//	Username      string         `json:"username" db:"username"`
//	CommunityName string         `json:"community_name" db:"community_name" binding:"required"`
//	Title         string         `json:"title" db:"title" binding:"required"`
//	Content       string         `json:"content" db:"content" binding:"required"`
//	CreatedAt     time.Time      `json:"create_time" db:"created_at"`
//	UpdatedAt     time.Time      `json:"update_time" db:"updated_at"`
//	DeletedAt     gorm.DeletedAt `json:"deleted_time" db:"deleted_at"`
//}

//帖子详情（帖子，社区，作者）结构体
type ApiPostDetail struct {
	VoteNum    int64  `json:"vote_num"`
	AuthorName string `json:"author_name" db:"username"`

	*Post                               //嵌入 帖子结构体
	*CommunityDetail `json:"community"` //嵌入 社区信息
}
