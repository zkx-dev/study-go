# goLand右侧 Database -> bluebell -> Run SQL script 选择要执行的sql文件
DROP TABLE IF EXISTS `community`;
CREATE TABLE `community` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `community_id` int(10) unsigned NOT NULL,
     `community_name` varchar(128) COLLATE utf8mb4_general_ci NOT NULL,
     `introduction` varchar(256) COLLATE utf8mb4_general_ci NOT NULL,
     `created_at` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
     `updated_at` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
     `deleted_at` datetime(0) NULL DEFAULT NULL COMMENT '删除日期',
     PRIMARY KEY (`id`),
     UNIQUE KEY `idx_community_id` (`community_id`),
     UNIQUE KEY `idx_community_name` (`community_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
INSERT INTO `community` VALUES ('1', '1', 'Go', 'Golang', '2016-11-01 08:10:10', '2016-11-01 08:10:10',null);
INSERT INTO `community` VALUES ('2', '2', 'leetcode', '刷题刷题刷题', '2020-01-01 08:00:00', '2020-01-01 08:00:00',null);
INSERT INTO `community` VALUES ('3', '3', '计算机网络', '介绍计算机网络面试知识', '2022-08-12 08:00:00', '2022-08-12 08:00:00',null);