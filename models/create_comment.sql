DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
       `id` bigint(20) NOT NULL AUTO_INCREMENT,
       `comment_id` bigint(20) unsigned NOT NULL,
       `content` text COLLATE utf8mb4_general_ci NOT NULL,
       `post_id` bigint(20) NOT NULL,
       `author_id` bigint(20) NOT NULL,
       `parent_id` bigint(20) NOT NULL DEFAULT '0',
       `created_at` datetime(0) NULL DEFAULT NULL COMMENT '创建日期',
       `updated_at` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
       `deleted_at` datetime(0) NULL DEFAULT NULL COMMENT '删除日期',
       PRIMARY KEY (`id`),
       UNIQUE KEY `idx_comment_id` (`comment_id`),
       KEY `idx_author_Id` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;