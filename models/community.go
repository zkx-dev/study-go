package models

import "time"

type Community struct {
	Id            int64  `json:"id" db:"community_id"`
	CommunityName string `json:"name" db:"community_name"`
}
type CommunityDetail struct {
	CommunityId   int64     `json:"id" db:"community_id"`
	CommunityName string    `json:"name" db:"community_name"`
	Introduction  string    `json:"introduction,omitempty" db:"introduction"`
	CreatedAt     time.Time `json:"create_time" db:"created_at"`
}
