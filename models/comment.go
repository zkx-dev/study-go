package models

import (
	"time"

	"gorm.io/gorm"
)

type Comment struct {
	PostId    uint64         `json:"id,string" db:"post_id"`
	ParentId  uint64         `db:"parent_id" json:"parent_id"`
	CommentId uint64         `json:"comment_id" db:"comment_id"`
	AuthorId  uint64         `db:"author_id" json:"author_id"`
	Content   string         `db:"content" binding:"required" json:"content"`
	CreatedAt time.Time      `db:"created_at" json:"create_time"`
	UpdatedAt time.Time      `db:"updated_at" json:"update_time"`
	DeletedAt gorm.DeletedAt `db:"deleted_time" json:"deleted_at"`
}
