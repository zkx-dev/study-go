package models

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	UserId   uint64 `db:"user_id,string"`
	Username string `db:"username"`
	Password string `db:"password"`
	Email    string `db:"email"`
	// `gorm:"-" 忽略该字段`
	Token     string    `gorm:"-"`
	Gender    int       `db:"gender"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
	/**gorm.DeletedAt 可以实现软删除
	拥有软删除能力的模型 调用Delete时，记录不会从数据库删除，
	而是将该记录的DeletedAt 置为 当前时间，并且无法通过普通查询查到
	使用Unscoped找到被软删除的记录
	*/
	DeletedAt gorm.DeletedAt `db:"deleted_at"`
}
