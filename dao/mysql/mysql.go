package mysql

import (
	"fmt"
	"os"
	"web_app/setting"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var db *gorm.DB

//新版代码
func Init(cfg *setting.MySQLConfig) (err error) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		cfg.User,
		cfg.Password,
		cfg.Host,
		cfg.Port,
		cfg.DbName,
	)
	//db ,err := sqlx.Connect("mysql",dsn)
	//if err != nil {
	//	fmt.Printf("connect DB failed, err:%v\n",err)
	//	return
	//}
	//db.SetMaxOpenConns(20)
	//db.SetMaxIdleConns(10)
	db, err = gorm.Open(mysql.New(mysql.Config{
		DriverName: "mysql",
		DSN:        dsn,
	}), &gorm.Config{})
	//db, err = gorm.Open("mysql", dsn)
	if err != nil {
		fmt.Printf("connect DB failed, err:%v\n", err)
		return
	}
	sqlDB, _ := db.DB()
	//设置连接池中 “打开”连接的最大值 “打开”连接 = "正在使用"的连接 + "空闲"的连接
	sqlDB.SetMaxOpenConns(cfg.MaxOpenConnns)
	//设置空闲连接池中的最大连接数
	sqlDB.SetMaxOpenConns(cfg.MaxIdleConnns)
	return
}

//旧版代码
//func Init() (err error) {
//
//	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True",
//		viper.GetString("mysql.user"),
//		viper.GetString("mysql.password"),
//		viper.GetString("mysql.host"),
//		viper.GetInt("mysql.port"),
//		viper.GetString("mysql.db_name"),
//	)
//	//db ,err := sqlx.Connect("mysql",dsn)
//	//if err != nil {
//	//	fmt.Printf("connect DB failed, err:%v\n",err)
//	//	return
//	//}
//	//db.SetMaxOpenConns(20)
//	//db.SetMaxIdleConns(10)
//	db, err = gorm.Open("mysql", dsn)
//	if err != nil {
//		fmt.Printf("connect DB failed, err:%v\n", err)
//		return
//	}
//	sqlDB := db.DB()
//	//设置连接池中 “打开”连接的最大值 “打开”连接 = "正在使用"的连接 + "空闲"的连接
//	sqlDB.SetMaxOpenConns(viper.GetInt("mysql.max_open_conns"))
//	//设置空闲连接池中的最大连接数
//	sqlDB.SetMaxOpenConns(viper.GetInt("mysql.max_idle_conns"))
//	return
//}

func Close() {

	os.Exit(0)
}
