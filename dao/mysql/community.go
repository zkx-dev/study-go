package mysql

import (
	"web_app/models"

	"github.com/jinzhu/gorm"
	"go.uber.org/zap"
)

func GetCommunityList() (communityList []*models.Community, err error) {
	err = db.Table("community").Select([]string{"community_id", "community_name"}).Find(&communityList).Error
	if gorm.IsRecordNotFoundError(err) {
		zap.L().Warn("there is no community in db")
		err = nil
	}
	return
}

func GetCommunityDetailById(id int64) (communityDetail *models.CommunityDetail, err error) {
	communityDetail = new(models.CommunityDetail)
	err = db.Table("community").
		Select([]string{"community_id", "community_name", "introduction", "created_at"}).
		Where("community_id = ?", id).Find(communityDetail).Error
	if gorm.IsRecordNotFoundError(err) {
		err = ErrorInvalidId
	}
	return
}
