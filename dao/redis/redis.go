package redis

import (
	"fmt"
	"web_app/setting"

	"github.com/go-redis/redis"
)

var rdb *redis.Client

//新版代码
func Init(cfg *setting.RedisConfig) (err error) {
	rdb = redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%d",
			cfg.Host,
			cfg.Port,
		),
		Password: cfg.Password,
		DB:       cfg.DB,
		PoolSize: cfg.PoolSize,
	})
	_, err = rdb.Ping().Result()
	return
}

//旧版代码
//func Init() (err error) {
//	rdb = redis.NewClient(&redis.Options{
//		Addr: fmt.Sprintf("%s:%d",
//			viper.GetString("redis.host"),
//			viper.GetInt("redis.port"),
//		),
//		Password: viper.GetString("redis.password"),
//		DB:       viper.GetInt("redis.db"),
//		PoolSize: viper.GetInt("redis.pool_size"),
//	})
//	_, err = rdb.Ping().Result()
//	return
//}

func Close() {
	_ = rdb.Close()
}
