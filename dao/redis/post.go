package redis

import (
	"strconv"
	"time"
	"web_app/models"

	"github.com/go-redis/redis"
)

func getIdsByPagination(key string, page, limit int64) ([]string, error) {
	//2. 确定查询的索引起始点
	/**
	start = (当前页码-1)*每页记录条数
	end = start+每页记录条数-1
	*/
	start := (page - 1) * limit
	end := start + limit - 1
	//3.ZRevRange 按分数从大到小顺序 查询指定数量的元素
	//fmt.Println(rdb.ZRevRange(key, start, end))
	return rdb.ZRevRange(key, start, end).Result()
}

func GetPostIdListInOrder(p *models.Pagination) ([]string, error) {
	//根据用户请求参数中携带的sort参数确定要查询的redis key
	key := getRedisKey(keyPostTimeZSet)
	if p.Sort == models.Score {
		key = getRedisKey(keyPostScoreZSet)
	}
	return getIdsByPagination(key, p.Page, p.Limit)
}

func GetPostIdListInCommunityId(cp *models.Pagination) ([]string, error) {
	//使用zinterstore 把分区的帖子set 与 帖子分数的zset 取交集 生成一个新的zset
	//针对新的zset 按之前的逻辑取数据

	//社区的key
	cKey := getRedisKey(KeyCommunitySetPF + strconv.Itoa(int(cp.CommunityId)))
	//根据用户请求参数中携带的sort参数确定要查询的redis key
	key := getRedisKey(keyPostTimeZSet) + strconv.Itoa(int(cp.CommunityId))
	param := getRedisKey(keyPostTimeZSet)
	if cp.Sort == models.Score {
		key = getRedisKey(keyPostScoreZSet) + strconv.Itoa(int(cp.CommunityId))
		param = getRedisKey(keyPostScoreZSet)
	}
	//利用缓存key 减少zinterstore执行的次数
	if rdb.Exists(key).Val() < 1 {
		//不存在，需要计算
		pipeline := rdb.Pipeline()
		pipeline.ZInterStore(key, redis.ZStore{
			Aggregate: "MAX",
		}, cKey, param) //将cKey和param 中 键相同的列 以及对应的值 组成一个新的zset 叫key
		pipeline.Expire(key, 5*time.Second) //设置超时时间
		_, err := pipeline.Exec()
		if err != nil {
			return nil, err
		}
	}
	//存在的话，直接根据key 查询idList
	return getIdsByPagination(key, cp.Page, cp.Limit)

}

//根据idList 查询每篇帖子的得票总数（赞成票-反对票）
func GetPostVoteData(idList []string) (data []int64, err error) {
	data = make([]int64, 0, len(idList))
	for _, id := range idList {
		key := getRedisKey(keyPostVotedZSetPF + id)
		//查询指定帖子的投票数量 赞成票数量v1 - 反对票数量v2
		v1 := rdb.ZCount(key, "1", "1").Val()
		v2 := rdb.ZCount(key, "-1", "-1").Val()
		data = append(data, v1-v2)
	}
	return
}
