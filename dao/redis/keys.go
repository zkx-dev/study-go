package redis

const (
	keyPrefix          = "bluebell:"
	keyPostTimeZSet    = "post:time"   //zset 帖子id:帖子的创建时间
	keyPostScoreZSet   = "post:score"  //zset 帖子id:帖子投票分数
	keyPostVotedZSetPF = "post:voted:" //zset 用户id:1/-1 参数是post_id
	KeyCommunitySetPF  = "community:"  //set 保存每个分区下帖子的id

)

//给redis key加上前缀
func getRedisKey(key string) string {
	return keyPrefix + key
}
