package controller

import (
	"errors"
	"fmt"
	"strings"
	"web_app/dao/mysql"
	"web_app/logic"
	"web_app/models"

	"github.com/go-playground/validator/v10"

	"go.uber.org/zap"

	"github.com/gin-gonic/gin"
)

//处理注册请求的函数
// RegisterHandler 注册业务
// @Summary 注册业务
// @Description 注册业务
// @Tags 用户业务接口
// @Accept application/json
// @Produce application/json
// @Param Authorization header string false "Bearer 用户令牌"
// @Param object query models.ParamUserRegister false "查询参数"
// @Security ApiKeyAuth
// @Success 200 {object} _ResponsePostList
// @Router /signup [POST]
func RegisterHandler(c *gin.Context) {
	//1.获取参数，参数校验
	p := new(models.ParamUserRegister)
	if err := c.ShouldBindJSON(p); err != nil {
		//记录日志
		//zap.L().Error(msg string, fields ...Field)
		zap.L().Error("Register with invalid param", zap.Error(err))

		//判断err是不是validator.ValidationErrors类型
		errs, ok := err.(validator.ValidationErrors)
		if !ok {
			ResponseError(c, CodeInvalidPassword)
			return
		}
		ResponseErrorWithMsg(c, CodeInvalidParam, removeTopStruct(errs.Translate(trans)))
		return
	}
	//fmt.Println(p)
	//2.业务处理
	err := logic.Register(p)
	if err != nil {
		zap.L().Error("logic.Register failed", zap.Error(err))
		//判断 该错误 是否为 目标错误
		if errors.Is(err, mysql.ErrorUserExist) {
			ResponseError(c, CodeUserExist)
		}
		ResponseError(c, CodeServerBusy)
		return
	}
	//3.返回响应
	ResponseSuccess(c, nil)
}

//去掉提示信息中的结构体名称
func removeTopStruct(fields map[string]string) map[string]string {
	res := map[string]string{}
	for field, err := range fields {
		res[field[strings.Index(field, ".")+1:]] = err
	}
	return res
}

//func SelectHandler(c *gin.Context) {
//	logic.Select()
//}
//处理登录请求的函数
// LoginHandler 登录业务
// @Summary 登录业务
// @Description 登录业务
// @Tags 用户业务接口
// @Accept application/json
// @Produce application/json
// @Param Authorization header string false "Bearer 用户令牌"
// @Param object query models.ParamUserLogin false "查询参数"
// @Security ApiKeyAuth
// @Success 200 {object} _ResponsePostList
// @Router /login [POST]
func LoginHandler(c *gin.Context) {
	p := new(models.ParamUserLogin)
	if err := c.ShouldBindJSON(p); err != nil {
		//记录日志
		//zap.L().Error(msg string, fields ...Field)
		zap.L().Error("Login with invalid param", zap.Error(err))

		//判断err是不是validator.ValidationErrors类型
		errs, ok := err.(validator.ValidationErrors)
		if !ok {
			ResponseError(c, CodeInvalidParam)
			return
		}
		ResponseErrorWithMsg(c, CodeInvalidParam, removeTopStruct(errs.Translate(trans)))
		return
	}
	//登录业务 逻辑处理
	user, err := logic.Login(p)
	if err != nil {
		//fmt.Println(err)
		zap.L().Error("logic.Login failed", zap.String("username", p.Username), zap.Error(err))
		if errors.Is(err, mysql.ErrorUserNotExist) {
			ResponseError(c, CodeUserNotExist)
		}
		ResponseError(c, CodeServerBusy)
		return
	}
	//3.返回响应
	ResponseSuccess(c, gin.H{
		"user_id":   fmt.Sprintf("%d", user.UserId),
		"user_name": user.Username,
		"token":     user.Token,
	})
}
