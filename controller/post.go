package controller

import (
	"strconv"
	"web_app/logic"
	"web_app/models"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// CreatePostHandler 创建帖子
// @Summary 创建帖子
// @Description 创建帖子
// @Tags 帖子相关接口
// @Accept application/json
// @Produce application/json
// @Param Authorization header string true "Bearer 用户令牌"
// @Param object query models.Post false "查询参数"
// @Security ApiKeyAuth
// @Success 200 {object} _ResponsePostList
// @Router /post [POST]
func CreatePostHandler(c *gin.Context) {
	p := new(models.Post)
	if err := c.ShouldBindJSON(p); err != nil {
		zap.L().Debug("c.ShouldBindJSON(p) error", zap.Any("err", err))
		zap.L().Error("create post with invalid params")
		ResponseError(c, CodeInvalidParam)
		return
	}
	//创建帖子
	//从上下文中获取到当前登录用户id 赋给post的AuthorId
	userId, err := GetCurrentUser(c)
	if err != nil {
		ResponseError(c, CodeNeedLogin)
		return
	}
	p.AuthorId = userId
	if err := logic.CreatePost(p); err != nil {
		zap.L().Error("logic.CreatePost(p) failed", zap.Error(err))
		ResponseError(c, CodeServerBusy)
		return
	}
	//返回响应
	ResponseSuccess(c, nil)
}

//帖子详情
// GetPostDetailHandler 根据Id查询帖子详情
// @Summary 升级版帖子列表接口
// @Description 可按社区按时间或分数排序查询帖子列表接口
// @Tags 帖子相关接口
// @Accept application/json
// @Produce application/json
// @Param Authorization header string true "Bearer 用户令牌"
// @Param object query postId  path    int     true        "id"
// @Security ApiKeyAuth
// @Success 200 {object} _ResponsePostList
// @Router /post/:id [get]
func GetPostDetailHandler(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.ParseUint(idStr, 10, 64)
	if err != nil {
		ResponseError(c, CodeInvalidParam)
		return
	}
	//data, err := logic.GetPostById(id)
	data, err := logic.GetPostDetailById(id)
	if err != nil {
		zap.L().Error("logic.GetPostDetailById(id) failed", zap.Error(err))
		ResponseError(c, CodeServerBusy)
		return
	}
	ResponseSuccess(c, data)
}

//根据指定排序方式 返回帖子列表（帖子创建时间 desc）
/**
具体逻辑
1.从url中获取参数
2.去redis查询id列表
3.根据id去数据库查询帖子详细信息
*/
func GetPostListHandler(c *gin.Context) {
	//获取分页参数
	pagination, err := GetPageInfo(c)
	if err != nil {
		zap.L().Error("redis.GetPageInfo(c) failed", zap.Error(err))
		ResponseError(c, CodeServerBusy)
		return
	}
	//获取数据
	data, err := logic.GetPostList(&pagination)
	if err != nil {
		zap.L().Error("logic.GetPostList() failed", zap.Error(err))
		ResponseError(c, CodeServerBusy)
		return
	}
	ResponseSuccess(c, data)
}

//根据指定排序方式 返回帖子列表（帖子得分 desc）
// 根据前端传来的参数动态的获取帖子列表
// 按创建时间排序 或者 按照 分数排序
// 1、获取请求的query string 参数
// 2、去redis查询id列表
// 3、根据id去数据库查询帖子详细信息
// PostList2Handler 升级版帖子列表接口
// @Summary 升级版帖子列表接口
// @Description 可按社区按时间或分数排序查询帖子列表接口
// @Tags 帖子相关接口
// @Accept application/json
// @Produce application/json
// @Param Authorization header string true "Bearer 用户令牌"
// @Param object query models.ParamPostList false "查询参数"
// @Security ApiKeyAuth
// @Success 200 {object} _ResponsePostList
// @Router /posts2 [get]

func GetPostListByParameterHandler(c *gin.Context) {
	//获取分页参数
	pagination, err := GetPageInfo(c)
	if err != nil {
		zap.L().Error("redis.GetPageInfo(c) failed", zap.Error(err))
		ResponseError(c, CodeServerBusy)
		return
	}
	//获取数据
	data, err := logic.HasCommunityId(&pagination)
	if err != nil {
		zap.L().Error("logic.GetPostList() failed", zap.Error(err))
		ResponseError(c, CodeServerBusy)
		return
	}
	ResponseSuccess(c, data)
}
