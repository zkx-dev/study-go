package controller

import (
	"strconv"
	"web_app/logic"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

//跟社区相关的
// CommunityHandler 社区列表
// @Summary 社区列表
// @Description 社区列表
// @Tags 社区业务接口
// @Accept application/json
// @Produce application/json
// @Param Authorization header string true "Bearer 用户令牌"
// @Param object query models.Community false "查询参数"
// @Security ApiKeyAuth
// @Success 200 {object} _ResponsePostList
// @Router /community [get]
func CommunityHandler(c *gin.Context) {
	//查询到所有的社区 以列表形式返回
	data, err := logic.GetCommunityList()
	if err != nil {
		zap.L().Error("logic.GetCommunityList() failed", zap.Error(err))
		ResponseError(c, CodeServerBusy)
		return
	}
	ResponseSuccess(c, data)
}

//根据社区id查询社区分类详情
// CommunityDetailHandler 社区详情
// @Summary 社区详情
// @Description 社区详情
// @Tags 社区业务接口
// @Accept application/json
// @Produce application/json
// @Param Authorization header string true "Bearer 用户令牌"
// @Param object query communityId     path    int     true        "id"
// @Security ApiKeyAuth
// @Success 200 {object} _ResponsePostList
// @Router /community/:id [get]
func CommunityDetailHandler(c *gin.Context) {
	//从URL中 根据key获取val val为string类型
	idStr := c.Param("id")
	//strconv.ParseInt(字符串,数字进制,数据类型)
	//strconv.ParseInt(idStr, 10, 64) 将idStr 转换为10进制数，类型为int64
	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		ResponseError(c, CodeInvalidParam)
		return
	}
	//根据id获取社区详情
	data, err := logic.GetCommunityDetailById(id)
	if err != nil {
		zap.L().Error("logic.GetCommunityDetail() failed", zap.Error(err))
		ResponseError(c, CodeServerBusy)
		return
	}
	ResponseSuccess(c, data)
}
