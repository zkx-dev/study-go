package logic

import (
	"web_app/dao/mysql"
	"web_app/models"
)

//查询社区列表
func GetCommunityList() ([]*models.Community, error) {
	return mysql.GetCommunityList()
}

//根据社区id查询社区详情
func GetCommunityDetailById(id int64) (*models.CommunityDetail, error) {
	return mysql.GetCommunityDetailById(id)
}
