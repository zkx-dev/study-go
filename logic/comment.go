package logic

import (
	"web_app/dao/mysql"
	"web_app/models"
)

func GetCommentListById(id uint64) ([]*models.Comment, error) {
	return mysql.GetCommentListById(id)
}
