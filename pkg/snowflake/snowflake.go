package snowflake

import (
	"fmt"
	"time"
	"web_app/setting"

	"github.com/sony/sonyflake"
)

var (
	sonyFlake     *sonyflake.Sonyflake // 实例
	sonyMachineID uint16               // 机器ID
)

func getMachineID() (uint16, error) { // 返回全局定义的机器ID
	return sonyMachineID, nil
}

// 需传入 指定日期 和 当前的机器ID
func Init(cfg *setting.SnowFlakeConfig) (err error) {
	sonyMachineID = cfg.MachineId
	//time.Parse(日期格式,具体日期) 参数类型 均为string
	t, _ := time.Parse("2006-01-02", cfg.InitTime) // 初始化一个开始的时间

	/**
	type Settings struct {
		StartTime      time.Time
		MachineID      func() (uint16, error)
		CheckMachineID func(uint16) bool
	}
	*/
	settings := sonyflake.Settings{ // 生成全局配置
		StartTime: t,
		//MachineID 字段 是函数类型
		MachineID: getMachineID, // 指定机器ID
	}
	sonyFlake = sonyflake.NewSonyflake(settings) // 用配置生成sonyflake节点
	return
}

// GetID 返回生成的id值
//数据表中 bigint(20) 啥意思 就是该字段的值 最大是一个20位的数
//对应go中 类型为 uint64 也就是 该数用2进制表示 是64位2^64 的值 是一个无符号的20位的数
func GetID() (id uint64, err error) { // 拿到sonyflake节点生成id值
	if sonyFlake == nil {
		err = fmt.Errorf("snoy flake not inited")
		return
	}

	id, err = sonyFlake.NextID()
	return
}

//func main() {
//	if err := Init("2022-07-28", 1); err != nil {
//		fmt.Printf("Init failed,err:%v\n", err)
//		return
//	}
//	id, _ := GetID()
//	fmt.Println(id)
//}
